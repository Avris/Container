<?php
namespace Avris\Container;

interface ContainerInterface extends \Psr\Container\ContainerInterface
{
    public function set(string $name, $service, array $tags = []): self;

    public function setDefinition(string $name, $definition): self;

    public function setWithAlias($service, string $alias): self;

    public function has($name): bool;

    public function get($name);

    public function getByTag(string $tag): array;

    public function restart(string $serviceName): self;

    public function delete(string $serviceName): self;

    public function clear(): self;

    public function getParameter(string $name);
}
