<?php
namespace Avris\Container;

use Avris\Container\Exception\NotFoundException;
use Psr\Container\ContainerInterface;

class ServiceLocator implements ContainerInterface, \IteratorAggregate
{
    /** @var ContainerInterface */
    private $container;

    /** @var string[] */
    private $services;

    public function __construct(ContainerInterface $container, array $services)
    {
        $this->container = $container;
        $this->services = $services;
    }

    public function get($id)
    {
        if (!$this->has($id)) {
            throw new NotFoundException(sprintf('Service %s not accessible from the service locator', $id));
        }

        return $this->container->get($id);
    }

    public function has($id)
    {
        return in_array($id, $this->services, true);
    }

    public function getIterator()
    {
        foreach ($this->services as $serviceName) {
            yield $serviceName => $this->container->get($serviceName);
        }
    }

    public function keys(): array
    {
        return $this->services;
    }
}
