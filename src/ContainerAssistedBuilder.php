<?php
namespace Avris\Container;

class ContainerAssistedBuilder
{
    /** @var Container */
    protected $container;

    public function __construct()
    {
        $this->container = new Container();
    }

    public function registerExtension(ContainerBuilderExtension $extension): self
    {
        $extension->extend($this->container);

        return $this;
    }

    public function build(string $service)
    {
        return $this->container->get($service);
    }
}
