<?php
namespace Avris\Container;

use Avris\Bag\Bag;
use Avris\Bag\BagHelper;
use Avris\Container\Exception\ContainerException;
use Avris\Container\Exception\NotFoundException;
use Avris\Container\Parameters\ParameterProvider;
use Avris\Container\Parameters\SimpleParameterProvider;
use Avris\Container\Service\Resolver;
use Avris\Container\Service\ServiceDefinition;

class Container implements ContainerInterface
{
    /** @var Bag */
    protected $definitions;

    /** @var Bag */
    protected $services;

    /** @var Bag */
    protected $tags;

    /** @var bool[] */
    protected $loading = [];

    /** @var ParameterProvider */
    protected $parameterProvider;

    public function __construct(ParameterProvider $parametersProvider = null)
    {
        $this->definitions = new Bag();
        $this->services = new Bag();
        $this->tags = new Bag();
        $this->parameterProvider = $parametersProvider ?? new SimpleParameterProvider();
    }

    public function set(string $name, $service, array $tags = []): ContainerInterface
    {
        if ($this->isContainerService($name)) {
            throw new ContainerException('Cannot overwrite container service');
        }

        if ($service === null) {
            return $this->delete($name);
        }

        $this->services->set($name, $service);

        foreach ($tags as $tag) {
            $this->tags->appendToElement($tag, $name);
        }

        return $this;
    }

    public function setDefinition(string $name, $definition): ContainerInterface
    {
        if ($this->isContainerService($name)) {
            throw new ContainerException('Cannot overwrite container service');
        }

        if ($definition === null) {
            return $this->delete($name);
        }

        if (is_array($definition)) {
            if (!isset($definition['class']) && !isset($definition['resolve'])) {
                $definition['class'] = $name;
            }
            $definition = ServiceDefinition::fromArray($definition, $name);
        }

        $this->definitions->set($name, $definition);
        $this->services->delete($name);

        if ($definition instanceof ServiceDefinition) {
            foreach ($definition->getTags() as $tag) {
                $this->tags->appendToElement($tag, $name);
            }
        }

        return $this;
    }

    public function setWithAlias($service, string $alias): ContainerInterface
    {
        $class = get_class($service);
        $this->set($class, $service);

        if ($alias !== $class) {
            $this->setDefinition($alias, $class);
        }

        return $this;
    }

    public function has($name): bool
    {
        return $this->definitions->offsetExists($name)
            || $this->services->offsetExists($name)
            || $this->isContainerService($name);
    }

    public function get($name)
    {
        return $this->getInternal($name, false);
    }

    protected function getInternal(string $name, $isInternal = true)
    {
        list($optional, $name) = $this->handleOptional($name);

        $parts = explode('.', $name);
        $startingServiceName = $this->followAliases($parts[0]);
        $startingService = $this->isContainerService($startingServiceName)
            ? $this // @codeCoverageIgnore
            : $this->services->get($startingServiceName);

        if (!$startingService) {
            if ($startingService !== null) {
                return $startingService;
            }

            if (!$this->definitions->has($startingServiceName)) {
                return $this->handleServiceNotFound($name, $optional);
            }

            $definition = $this->checkPrivateService($startingServiceName, $isInternal);
            $startingService = $this->buildService($startingServiceName, $definition);
        } elseif (!$isInternal) {
            $this->checkPrivateService($startingServiceName, $isInternal);
        }

        $return = $startingService;

        for ($i = 1; $i < count($parts); $i++) {
            list($optional, $currentKey) = $this->handleOptional($parts[$i]);

            try {
                $return = BagHelper::magicGetter($return, $currentKey, BagHelper::THROW_EXCEPTION);
            } catch (\Avris\Bag\NotFoundException $e) {
                return $this->handleServiceNotFound($name, $optional);
            }
        }

        return $return;
    }

    protected function handleOptional(string $serviceName): array
    {
        return substr($serviceName, 0, 1) === '?'
            ? [true, substr($serviceName, 1)]
            : [false, $serviceName];
    }

    protected function followAliases(string $name): string
    {
        do {
            $definition = $this->definitions[$name] ?? null;
            if ($definition === null || $definition instanceof ServiceDefinition) {
                return $name;
            }
        } while ($name = $definition);
    } // @codeCoverageIgnore

    protected function checkPrivateService(string $name, bool $isInternal): ?ServiceDefinition
    {
        /** @var ServiceDefinition $definition */
        $definition = $this->definitions->get($name);

        if (!$isInternal && $definition && !$definition->isPublic()) {
            throw new NotFoundException(sprintf('Cannot directly access a private service "%s"', $name));
        }

        return $definition;
    }

    protected function handleServiceNotFound($serviceName, $optional)
    {
        if ($optional) {
            return null;
        }

        $requiredBy = count($this->loading)
            ? sprintf(' (required by %s)', implode(', ', array_keys(array_reverse($this->loading))))
            : '';

        throw new NotFoundException(sprintf('Service "%s" not found', $serviceName) . $requiredBy);
    }

    protected function buildService($serviceName, ServiceDefinition $definition)
    {
        $this->checkCircularDependency($serviceName);

        if ($definition->getResolve() !== null) {
            return $this->services[$serviceName] = $this->handleParameter($definition->getResolve());
        }

        $serviceObject = $this->instantiate($serviceName, $definition);

        if ($serviceObject instanceof Resolver) {
            $serviceObject = $serviceObject->resolve();
        }

        if (!$definition->isFactory()) {
            $this->services[$serviceName] = $serviceObject;
        }

        $this->applyCalls($serviceName, $definition, $serviceObject);

        return $serviceObject;
    }

    protected function checkCircularDependency($serviceName)
    {
        if (isset($this->loading[$serviceName])) {
            throw new ContainerException(sprintf(
                'Circular dependency detected between services: %s',
                implode(', ', array_keys($this->loading))
            ));
        }
    }

    protected function instantiate($serviceName, ServiceDefinition $serviceConfig)
    {
        try {
            $this->loading[$serviceName] = true;
            $reflector = new \ReflectionClass($serviceConfig->getClass());

            return $reflector->newInstanceArgs(
                $this->adjustParameters(
                    $serviceName, // @codeCoverageIgnore
                    $serviceConfig->getArguments(),
                    $reflector->getConstructor()
                )
            );
        } catch (\Exception $e) {
            throw $e;
        } finally {
            unset($this->loading[$serviceName]);
        }
    } // @codeCoverageIgnore

    protected function applyCalls($serviceName, ServiceDefinition $serviceConfig, $serviceObject)
    {
        if (empty($serviceConfig->getCalls())) {
            return;
        }

        $reflector = new \ReflectionObject($serviceObject);

        foreach ($serviceConfig->getCalls() as $call) {
            if (!is_array($call) || count($call) !== 2) {
                throw new ContainerException(sprintf(
                    'A call definition should be an array [method, [parameters, ...]] (service %s)',
                    $serviceName
                ));
            }

            list($method, $parameters) = $call;

            if (!$reflector->hasMethod($method)) {
                throw new ContainerException(sprintf(
                    'Call %s does not exist for service %s',
                    $method, // @codeCoverageIgnore
                    $serviceName
                ));
            }

            $params = array_map(function ($parameter) {
                return $this->handleParameter($parameter);
            }, BagHelper::toArray($parameters));

            $methodReflector = $reflector->getMethod($method);
            $methodReflector->invokeArgs(
                $serviceObject, // @codeCoverageIgnore
                $this->adjustParameters($serviceName, $params, $methodReflector)
            );
        }
    }

    protected function adjustParameters(string $serviceName, array $parameters, \ReflectionMethod $method = null)
    {
        $adjustedParameters = [];
        $expectedParameters = $method ? $method->getParameters() : [];

        foreach ($expectedParameters as $expected) {
            if (!isset($parameters['$' . $expected->getName()]) &&
                !isset($parameters[$expected->getPosition()])
            ) {
                if ($expected->isDefaultValueAvailable()) {
                    $adjustedParameters[$expected->getPosition()] = $expected->getDefaultValue();
                    continue;
                }

                throw new ContainerException(sprintf(
                    'Parameter $%s is required for service %s and cannot be autowired',
                    $expected->getName(),
                    $serviceName
                ));
            }

            $adjusted = $this->handleParameter(
                $parameters['$' . $expected->getName()] ?? $parameters[$expected->getPosition()]
            );

            if ($expected->getClass() && $expected->getClass()->getName() === Bag::class && !$adjusted instanceof Bag) {
                $adjusted = new Bag($adjusted);
            }

            $adjustedParameters[$expected->getPosition()] = $adjusted;
        }

        return $adjustedParameters;
    }

    protected function handleParameter($parameter)
    {
        if (!is_string($parameter)) {
            return $parameter;
        }

        $parameter = $this->parameterProvider->replaceParameters($parameter);

        $parameter = preg_replace_callback('#{(.*)}#Ui', function ($matches) {
            return (string) $this->handleParameter($matches[1]);
        }, $parameter);

        if (substr($parameter, 0, 1) === '#') {
            return $this->getByTag(substr($parameter, 1));
        }

        if (substr($parameter, 0, 1) === '@') {
            return $this->getInternal(substr($parameter, 1));
        }

        return $parameter;
    }

    protected function isContainerService(string $serviceName)
    {
        return in_array($serviceName, ['', 'container', static::class, ContainerInterface::class], true);
    }

    public function getByTag(string $tag): array
    {
        $services = [];
        foreach ($this->tags->get($tag, []) as $serviceName) {
            $services[$serviceName] = $this->getInternal($serviceName);
        }

        return $services;
    }

    public function restart(string $serviceName): ContainerInterface
    {
        $this->services->delete($serviceName);

        return $this;
    }

    public function delete(string $serviceName): ContainerInterface
    {
        $this->definitions->delete($serviceName);
        $this->services->delete($serviceName);

        $this->cleanupMap($this->tags, $serviceName);

        return $this;
    }

    protected function cleanupMap(Bag $map, $serviceName)
    {
        foreach ($map as $key => $values) {
            if (($index = array_search($serviceName, $values)) !== false) {
                unset($values[$index]);
                $map->set($key, $values);
            }
        }

        return $map;
    }

    public function clear(): ContainerInterface
    {
        $this->definitions->clear();
        $this->services->clear();
        $this->tags->clear();

        return $this;
    }

    public function getParameter(string $name)
    {
        return $this->parameterProvider->getParameter($name);
    }
}
