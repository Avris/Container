<?php
namespace Avris\Container;

use Avris\Bag\Bag;
use Avris\Container\Exception\AutowiringException;
use Avris\Container\Service\ServiceDefinition;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

final class ContainerCompiler
{
    /** @var ContainerInterface */
    private $container;

    /** @var array */
    private $config;

    /** @var array */
    private $autoconfiguration;

    /** @var ServiceDefinition[] */
    private $definitions = [];

    /** @var string[] */
    private $implementations = [];

    /** @var AutowiringException[] */
    private $autowiringExceptions = [];

    public function __construct(
        ContainerInterface $container,
        array $definitions,
        array $autoconfiguration = []
    ) {
        $this->container = $container;
        $this->config = $definitions;
        $this->autoconfiguration = $autoconfiguration;
    }

    public function compile(): array
    {
        $this->loadConfig();
        $this->autowireAll();
        $this->cleanup();

        if (count($this->autowiringExceptions)) {
            throw reset($this->autowiringExceptions);
        }

        return $this->definitions;
    }

    private function loadConfig()
    {
        foreach ($this->config as $name => $options) {
            if ($options === null) {
                unset($this->definitions[$name]);
                continue;
            }
            if (is_string($options)) {
                $this->addAlias($name, $options);
                continue;
            }

            $definition = ServiceDefinition::fromArray($options, $name);

            if ($definition->getDir()) {
                $this->registerDir($name, $definition);
                continue;
            }

            $this->addDefinition($name, $definition);
        }
    }

    private function registerDir(string $namespace, ServiceDefinition $definition)
    {
        $finder = (new Finder())
            ->files()
            ->in($definition->getDir())
            ->name('*.php')
            ->ignoreDotFiles(true)
            ->ignoreUnreadableDirs(true)
            ->ignoreVCS(true);

        foreach ($definition->getExclude() as $notPath) {
            $finder->notPath($notPath);
        }

        /** @var SplFileInfo $file */
        foreach ($finder as $file) {
            $className = $this->buildClassName($namespace, $file->getRelativePathname());
            $reflection = new \ReflectionClass($className);
            if (!$reflection->isInterface() && !$reflection->isAbstract()) {
                $this->addDefinition($className, $definition->createFromDir($className));
            }
        }
    }

    private function buildClassName(string $namespace, string $relativePathName)
    {
        return $namespace . strtr(substr($relativePathName, 0, -4), ['/' => '\\']);
    }

    private function addDefinition(string $name, ServiceDefinition $definition)
    {
        if ($existingDefinition = $this->getDefinition($name)) {
            $existingDefinition->merge($definition->toArray(), $name);
        } else {
            $definition->setClassIfNotGiven($name);
            $this->autoconfigure($definition);

            $this->definitions[$name] = $definition;
        }

        if ($definition->getClass()) {
            $reflection = new \ReflectionClass($definition->getClass());
            foreach ($reflection->getInterfaceNames() as $interface) {
                $this->implementations[$interface][] = $definition->getClass();
            }
            do {
                $this->implementations[$reflection->getName()][] = $definition->getClass();
            } while ($reflection = $reflection->getParentClass());
        }
    }

    private function addAlias(string $from, string $to)
    {
        $this->definitions[$from] = $to;
    }

    private function autoconfigure(ServiceDefinition $definition)
    {
        if (!$definition->getClass()) {
            return;
        }

        $reflection = new \ReflectionClass($definition->getClass());

        foreach ($this->autoconfiguration as $class => $defaultOptions) {
            if ($reflection->isSubclassOf($class)) {
                $definition->merge($defaultOptions);
            }
        }
    }

    private function autowireAll()
    {
        foreach ($this->definitions as $name => $definition) {
            if ($definition instanceof ServiceDefinition) {
                $this->autowire($name, $definition);
            }
        }
    }

    private function autowire(string $name, ServiceDefinition $definition)
    {
        if (!$definition->getClass()) {
            return;
        }

        $reflection = new \ReflectionClass($definition->getClass());
        $constructor = $reflection->getConstructor();
        if (!$constructor) {
            return;
        }

        foreach ($constructor->getParameters() as $parameter) {
            if ($definition->hasArgument($parameter->getName())) {
                $this->markUsages($definition, $definition->getArgument($parameter->getName()));
                continue;
            }

            try {
                $value = $this->autowireParameter($name, $definition, $parameter);
                if ($value !== null) {
                    $definition->setArgument($parameter->getName(), $value);
                }
            } catch (AutowiringException $e) {
                $this->autowiringExceptions[$name] = $e;
            }
        }
    }

    private function markUsages(ServiceDefinition $definition, $argument)
    {
        if (!is_string($argument)) {
            return;
        }

        preg_match_all('#@([A-Za-z0-9\\\\]+)#', $argument, $matches, PREG_SET_ORDER);

        foreach ($matches as list(,$used)) {
            if (isset($this->definitions[$used])) {
                $this->getDefinition($used)->used($definition);
            }
        }
    }

    private function autowireParameter(
        string $name,
        ServiceDefinition $definition,
        \ReflectionParameter $parameter
    ): ?string {
        if (!$parameter->getClass()) {
            return $this->autowireParameterNotClass($name, $parameter);
        }

        $className = $parameter->getClass()->getName();

        if ($className === Bag::class && substr($parameter->getName(), 0, 6) === 'config') {
            return '@config.' . strtr(lcfirst(substr($parameter->getName(), 6)), ['_' => '.']);
        }

        if ($dependencyDefinition = $this->getDefinition($className)) {
            $dependencyDefinition->used($definition);

            return '@' . $className;
        }

        if ($this->container->has($className)) {
            return '@' . $className;
        }

        if ($parameter->isOptional()) {
            return null;
        }

        $implementations = $this->implementations[$className] ?? [];

        if (count($implementations) === 0
            && !$parameter->getClass()->isInterface()
            && !$parameter->getClass()->isAbstract()
        ) {
            $dependencyDefinition = $this->buildAutowiredService($className);
            $dependencyDefinition->used($definition);

            return '@' . $className;
        }

        throw new AutowiringException(sprintf(
            'Cannot autowire parameter $%s of service %s. ' .
            'Either wire the value manually or create an alias (candidates: %s)',
            $parameter->getName(),
            $name, // @codeCoverageIgnore
            join(', ', $implementations)
        ));
    }

    private function autowireParameterNotClass(string $name, \ReflectionParameter $parameter)
    {
        if ($parameter->getType()
            && $parameter->getType()->getName() === 'array'
            && substr($parameter->getName(), -1) === 's'
        ) {
            return '#' . substr($parameter->getName(), 0, -1);
        }

        if (preg_match('#^env([A-Z][A-Za-z]*)$#Uu', $parameter->getName(), $matches)) {
            return '%' . strtoupper(preg_replace('#([^\\^])([A-Z])#Uu', '$1_$2', $matches[1])) . '%';
        }

        if ($parameter->isDefaultValueAvailable()) {
            return null;
        }

        throw new AutowiringException(sprintf(
            'Cannot autowire parameter $%s of service %s',
            $parameter->getName(),
            $name
        ));
    }

    private function getDefinition(string $name): ?ServiceDefinition
    {
        do {
            $definition = $this->definitions[$name] ?? null;
            if ($definition === null || $definition instanceof ServiceDefinition) {
                return $definition;
            }
        } while ($name = $definition);
    } // @codeCoverageIgnore

    private function buildAutowiredService(string $className): ServiceDefinition
    {
        $definition = ServiceDefinition::fromArray([]);
        $this->addDefinition($className, $definition);
        $this->autowire($className, $definition);

        return $definition;
    }

    private function cleanup()
    {
        foreach ($this->definitions as $name => $definition) {
            if ($definition instanceof ServiceDefinition) {
                if (!$definition->shouldStay()) {
                    unset($this->definitions[$name]);
                    unset($this->autowiringExceptions[$name]);
                }
            }
        }

        foreach ($this->definitions as $name => $definition) {
            if ($this->getDefinition($name) === null) {
                $this->removeAliases($name);
            }
        }
    }

    private function removeAliases(string $name)
    {
        do {
            $definition = $this->definitions[$name] ?? null;
            unset($this->definitions[$name]);
        } while ($name = $definition);
    }
}
