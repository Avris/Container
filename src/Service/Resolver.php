<?php
namespace Avris\Container\Service;

interface Resolver
{
    public function resolve();
}
