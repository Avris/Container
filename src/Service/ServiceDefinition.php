<?php
namespace Avris\Container\Service;

use Avris\Bag\BagHelper;
use Avris\Bag\Set;

final class ServiceDefinition
{
    /** @var string|null */
    private $name;

    /** @var string|null */
    private $class;

    /** @var mixed[] */
    private $arguments = [];

    /** @var string[] */
    private $calls = [];

    /** @var string[] */
    private $tags = [];

    /** @var string|null */
    private $dir;

    /** @var string[] */
    private $exclude = [];

    /** @var bool */
    private $factory = false;

    /** @var mixed|null */
    private $resolve;

    /** @var bool */
    private $public = false;

    /** @var ServiceDefinition[] */
    private $usages = [];

    /** @var Set */
    private $ownFields;

    private function __construct()
    {
        $this->ownFields = new Set();
    }

    public static function fromArray(array $options, ?string $name = null): self
    {
        $definition = new static();

        $definition->name = isset($options['dir']) ? null : $name;

        foreach ($options as $name => $value) {
            switch ($name) {
                case 'class':
                    $definition->class = (string) $value;
                    break;
                case 'arguments':
                    $definition->arguments = BagHelper::toArray($value);
                    break;
                case 'calls':
                    $definition->calls = BagHelper::toArray($value);
                    break;
                case 'tags':
                    $definition->tags = BagHelper::toArray($value);
                    break;
                case 'dir':
                    $definition->dir = (string) $value;
                    break;
                case 'exclude':
                    $definition->exclude = BagHelper::toArray($value);
                    break;
                case 'factory':
                    $definition->factory = (bool) $value;
                    break;
                case 'resolve':
                    $definition->resolve = $value;
                    break;
                case 'public':
                    $definition->public = (bool) $value;
                    break;
            }

            $definition->ownFields->add($name);
        }

        return $definition;
    }

    public function merge(array $options, ?string $name = null): self
    {
        if ($name) {
            $this->name = $name;
        }

        foreach ($options as $name => $value) {
            switch ($name) {
                case 'class':
                    $this->class = (string) $value;
                    break;
                case 'arguments':
                    $this->arguments = array_merge($this->arguments, BagHelper::toArray($value));
                    break;
                case 'calls':
                    $this->calls = array_merge($this->calls, BagHelper::toArray($value));
                    break;
                case 'tags':
                    $this->tags = array_merge($this->tags, BagHelper::toArray($value));
                    break;
                case 'dir':
                    $this->dir = (string) $value;
                    break;
                case 'exclude':
                    $this->exclude = array_merge($this->exclude, BagHelper::toArray($value));
                    break;
                case 'factory':
                    $this->factory = (bool) $value;
                    break;
                case 'resolve':
                    $this->resolve = $value;
                    break;
                case 'public':
                    $this->public = (bool) $value;
                    break;
            }

            $this->ownFields->add($name);
        }

        return $this;
    }

    public function createFromDir(string $className): self
    {
        $newDefinition = clone $this;
        $newDefinition->class = $className;
        $newDefinition->dir = null;
        $newDefinition->exclude = [];

        $newDefinition->ownFields->add('class')->delete('dir')->delete('exclude');

        return $newDefinition;
    }

    public function setClassIfNotGiven(string $className): self
    {
        if (!$this->class && $this->resolve === null) {
            $this->class = $className;
            $this->ownFields->add('class');
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->dir ? null : ($this->name ?? $this->class);
    }

    public function getClass(): ?string
    {
        return $this->class;
    }

    public function getArguments(): array
    {
        return $this->arguments;
    }

    public function hasArgument(string $name)
    {
        return isset($this->arguments['$' . $name]);
    }

    public function getArgument($name)
    {
        return $this->arguments['$' . $name] ?? null;
    }

    public function setArgument($name, $value): self
    {
        $this->arguments['$' . $name] = $value;

        $this->ownFields->add('arguments');

        return $this;
    }

    public function getCalls(): array
    {
        return $this->calls;
    }

    public function getTags(): array
    {
        return $this->tags;
    }

    public function getDir(): ?string
    {
        return $this->dir;
    }

    public function getExclude(): array
    {
        return $this->exclude;
    }

    public function isFactory(): bool
    {
        return $this->factory;
    }

    public function getResolve()
    {
        return $this->resolve;
    }

    public function isPublic(): bool
    {
        return $this->public;
    }

    public function used(ServiceDefinition $definition): self
    {
        $this->usages[$definition->getName()] = $definition;

        return $this;
    }

    /**
     * @return ServiceDefinition[]
     */
    public function getUsages(): array
    {
        return $this->usages;
    }

    public function shouldStay(): bool
    {
        return $this->public || $this->isUsed() || !empty($this->tags);
    }

    private function isUsed(): bool
    {
        foreach ($this->usages as $usage) {
            if ($usage->shouldStay()) {
                return true;
            }
        }

        return false;
    }

    public function toArray()
    {
        $options = [
            'class' => $this->class,
            'arguments' => $this->arguments,
            'calls' => $this->calls,
            'tags' => $this->tags,
            'dir' => $this->dir,
            'exclude' => $this->exclude,
            'factory' => $this->factory,
            'resolve' => $this->resolve,
            'public' => $this->public,
        ];

        return array_filter($options, function ($key) {
            return in_array($key, $this->ownFields->all(), true);
        }, ARRAY_FILTER_USE_KEY);
    }
}
