<?php
namespace Avris\Container;

interface ContainerBuilderExtension
{
    public function extend(ContainerInterface $container);
}
