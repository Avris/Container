<?php
namespace Avris\Container\Parameters;

use Avris\Container\Exception\NotFoundException;

class SimpleParameterProvider implements ParameterProvider
{
    /** @var string[] */
    protected $parameters;

    public function __construct(array $parameters = [])
    {
        $this->parameters = $parameters;
    }

    public function replaceParameters(string $content, $context = null): string
    {
        return preg_replace_callback('#%([A-Za-z0-9_]+)%#', function ($matches) {
            return $this->getParameter($matches[1]);
        }, $content);
    }

    public function getParameter(string $name, $context = null)
    {
        if (!isset($this->parameters[$name])) {
            throw new NotFoundException(sprintf('Undefined parameter "%s"', $name));
        }

        return $this->parameters[$name];
    }
}
