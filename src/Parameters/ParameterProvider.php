<?php

namespace Avris\Container\Parameters;

interface ParameterProvider
{
    public function replaceParameters(string $content, $context = null): string;

    public function getParameter(string $name, $context = null);
}
