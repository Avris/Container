<?php

namespace Avris\Container;

use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Container\ServiceLocator
 */
class ServiceLocatorTest extends TestCase
{
    /** @var ServiceLocator */
    private $locator;

    protected function setUp()
    {
        $container = new Container();
        $container->set('foo', 'abc');
        $container->set('bar', 'def');
        $container->set('secret', 'XYZ');

        $this->locator = new ServiceLocator($container, ['foo', 'bar']);
    }

    public function testGet()
    {
        $this->assertEquals('abc', $this->locator->get('foo'));
        $this->assertEquals('def', $this->locator->get('bar'));
    }

    /**
     * @expectedException \Avris\Container\Exception\NotFoundException
     * @expectedExceptionMessage Service secret not accessible from the service locator
     */
    public function testGetNotAllowed()
    {
        $this->locator->get('secret');
    }

    /**
     * @expectedException \Avris\Container\Exception\NotFoundException
     * @expectedExceptionMessage Service nonexistent not accessible from the service locator
     */
    public function testGetNotExist()
    {
        $this->locator->get('nonexistent');
    }

    public function testHas()
    {
        $this->assertTrue($this->locator->has('foo'));
        $this->assertTrue($this->locator->has('bar'));
        $this->assertFalse($this->locator->has('secret'));
        $this->assertFalse($this->locator->has('nonexistent'));
    }

    public function testKeys()
    {
        $this->assertEquals(['foo', 'bar'], $this->locator->keys());
    }

    public function testIterator()
    {
        $services = iterator_to_array($this->locator);

        $this->assertEquals([
            'foo' => 'abc',
            'bar' => 'def',
        ], $services);
    }
}
