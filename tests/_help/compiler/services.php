<?php

return [
    'TestProjectCompiler\\' => [
        'dir' => __DIR__ . '/src/',
        'exclude' => [
            '#^Excluded#',
        ],
    ],

    \TestProjectCompiler\Bar::class => [
        'public' => true,
    ],

    \TestProjectCompiler\Thing\SecondThing::class => [
        'arguments' => [
            '$envValue' => 13,
        ]
    ],

    \TestProjectCompiler\Thing\ThingManagerInterface::class => \TestProjectCompiler\Thing\ThingManager::class,
    \TestProjectCompiler\Thing\ThingManager::class => [
        'public' => true,
    ],

    'val' => [
        'resolve' => 5,
    ],

    \TestProjectCompiler\Thing\FourthThing::class => null,
];
