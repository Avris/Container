<?php

namespace TestProjectCompiler;

class Bar
{
    /** @var Foo */
    private $foo;

    public function __construct(Foo $foo)
    {
        $this->foo = $foo;
    }

    public function getValue(): int
    {
        return $this->foo->getValue() + 15;
    }
}
