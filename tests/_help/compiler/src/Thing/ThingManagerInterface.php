<?php

namespace TestProjectCompiler\Thing;

interface ThingManagerInterface
{
    public function getValue(): int;

    public function getOptional(): ?\PDO;
}