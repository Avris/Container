<?php

namespace TestProjectCompiler\Thing;

interface ThingInterface
{
    public function getValue(): int;
}
