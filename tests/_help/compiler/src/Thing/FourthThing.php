<?php

namespace TestProjectCompiler\Thing;

class FourthThing implements ThingInterface
{
    public function getValue(): int
    {
        return 666;
    }
}
