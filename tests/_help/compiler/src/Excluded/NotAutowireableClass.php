<?php

namespace TestProjectCompiler\Excluded;

use TestProjectCompiler\Thing\ThingInterface;

class NotAutowireableClass
{
    public function __construct(ThingInterface $thing)
    {
    }
}
