<?php

namespace TestProjectCompiler\Excluded;

use Avris\Bag\Bag;
use TestProjectExternal\Dependency;
use TestProjectExternal\External;

class Config
{
    /** @var string */
    private $value;

    public function __construct(Dependency $manual, Bag $configFoo_bar)
    {
        $this->value = $manual->getValue() . '-' . $configFoo_bar->get('baz');
    }

    public function getValue()
    {
        return $this->value;
    }
}
