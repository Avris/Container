<?php

namespace TestProjectCompiler\Excluded;

use TestProjectExternal\External;

class AutowireDependency
{
    /** @var External */
    private $thing;

    public function __construct(External $thing)
    {
        $this->thing = $thing;
    }

    public function getValue()
    {
        return $this->thing->getValue() * 11;
    }
}
