<?php

namespace TestProjectContainer;

class CircularB
{
    /** @var CircularA */
    private $a;

    public function __construct(CircularA $a)
    {
        $this->a = $a;
    }
}
