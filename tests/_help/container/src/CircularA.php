<?php

namespace TestProjectContainer;

class CircularA
{
    /** @var CircularB */
    private $b;

    public function __construct(CircularB $b)
    {
        $this->b = $b;
    }
}
