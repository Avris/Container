<?php

namespace TestProjectContainer;

class Factory
{
    /** @var int */
    private $value;

    public function __construct()
    {
        $this->value = rand(0, 100000);
    }

    public function getValue(): int
    {
        return $this->value;
    }
}
