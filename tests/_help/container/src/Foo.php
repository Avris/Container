<?php

namespace TestProjectContainer;

class Foo
{
    /** @var int */
    private $value;

    public function __construct(int $envValue)
    {
        $this->value = $envValue;
    }

    public function getValue(): int
    {
        return $this->value;
    }
}
