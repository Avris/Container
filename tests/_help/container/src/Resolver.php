<?php

namespace TestProjectContainer;

class Resolver implements \Avris\Container\Service\Resolver
{
    /** @var Foo */
    private $foo;

    public function __construct(Foo $foo)
    {
        $this->foo = $foo;
    }

    public function resolve()
    {
        return 'OK-' . $this->foo->getValue();
    }
}
