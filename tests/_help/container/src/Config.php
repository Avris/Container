<?php

namespace TestProjectContainer;

use Avris\Bag\Bag;

class Config
{
    /** @var Bag */
    private $config;

    /** @var string */
    private $required;

    /** @var string|null */
    private $optionalExistent;

    /** @var string|null */
    private $optionalNonexistent;

    /** @var string|null */
    private $additional;

    public function __construct(
        Bag $config,
        string $required,
        string $optionalExistent = null,
        string $optionalNonexistent = null
    )
    {
        $this->config = $config;
        $this->required = $required;
        $this->optionalExistent = $optionalExistent;
        $this->optionalNonexistent = $optionalNonexistent;
    }

    public function getConfig(): Bag
    {
        return $this->config;
    }

    public function getRequired(): string
    {
        return $this->required;
    }

    public function getOptionalExistent(): ?string
    {
        return $this->optionalExistent;
    }

    public function getOptionalNonexistent(): ?string
    {
        return $this->optionalNonexistent;
    }

    public function setAdditional(string $additional)
    {
        $this->additional = $additional;

        return $this;
    }

    public function getAdditional(): ?string
    {
        return $this->additional;
    }
}
