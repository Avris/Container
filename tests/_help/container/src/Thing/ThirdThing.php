<?php

namespace TestProjectContainer\Thing;

class ThirdThing implements ThingInterface
{
    /** @var int */
    private $value;

    public function __construct(int $value = 7)
    {
        $this->value = $value;
    }

    public function getValue(): int
    {
        return $this->value;
    }
}
