<?php

namespace TestProjectContainer\Thing;

interface ThingManagerInterface
{
    public function getValue(): int;

    public function getOptional(): ?\PDO;
}