<?php

namespace TestProjectContainer\Thing;

class ThingManager implements ThingManagerInterface
{
    /** @var array|ThingInterface[] */
    private $things;

    /** @var \PDO|null */
    private $optional;

    public function __construct(array $things, \PDO $optional = null)
    {
        $this->things = $things;
        $this->optional = $optional;
    }

    public function getValue(): int
    {
        $value = 0;

        foreach ($this->things as $thing) {
            $value += $thing->getValue();
        }

        return $value;
    }

    public function getOptional(): ?\PDO
    {
        return $this->optional;
    }
}
