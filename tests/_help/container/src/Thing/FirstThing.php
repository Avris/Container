<?php

namespace TestProjectContainer\Thing;

class FirstThing implements ThingInterface
{
    /** @var int */
    private $value;

    public function __construct(int $envValue)
    {
        $this->value = $envValue;
    }

    public function getValue(): int
    {
        return 3 * $this->value;
    }
}
