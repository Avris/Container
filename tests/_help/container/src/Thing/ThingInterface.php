<?php

namespace TestProjectContainer\Thing;

interface ThingInterface
{
    public function getValue(): int;
}
