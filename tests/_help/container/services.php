<?php

return [
    'TestProjectContainer\\' => [
        'dir' => __DIR__ . '/src/',
        'exclude' => [
            '#^Config#',
            '#^BrokenParam#',
        ],
    ],

    \TestProjectContainer\Bar::class => [
        'public' => true,
    ],

    'barValue' => [
        'resolve' => '@TestProjectContainer\Bar.value',
        'public' => true,
    ],

    \TestProjectContainer\Thing\SecondThing::class => [
        'arguments' => [
            '$envValue' => 13,
        ]
    ],

    \TestProjectContainer\Thing\ThingManagerInterface::class => \TestProjectContainer\Thing\ThingManager::class,
    \TestProjectContainer\Thing\ThingManager::class => [
        'public' => true,
    ],

    'ConfigClass' => [
        'class' => \TestProjectContainer\Config::class,
        'arguments' => [
            '$config' => '@config',
            '$required' => '@config.foo',
            '$optionalExistent' => '@config.?bar',
            '$optionalNonexistent' => '@config.?baz',
        ],
        'public' => true,
        'calls' => [
            ['setAdditional', ['OŚM-{@config.foo}-%VALUE%']]
        ],
    ],

    \TestProjectContainer\BrokenDependency::class => [
        'public' => true,
        'arguments' => [
            '$nonexistent' => '@Nonexistent',
        ],
    ],
    \TestProjectContainer\BrokenCall::class => [
        'public' => true,
        'calls' => [
            ['nonexistent', ['foo']],
        ],
    ],
    \TestProjectContainer\BrokenCall2::class => [
        'public' => true,
        'calls' => [
            'xxx' => ['foo'],
        ],
    ],

    \TestProjectContainer\CircularA::class => [
        'public' => true,
    ],
    \TestProjectContainer\CircularB::class => [
        'public' => true,
    ],

    \TestProjectContainer\Resolver::class => [
        'public' => true,
    ],

    'zero' => [
        'public' => true,
        'resolve' => 0,
    ],

    \TestProjectContainer\Factory::class => [
        'public' => true,
        'factory' => true,
    ],
];
