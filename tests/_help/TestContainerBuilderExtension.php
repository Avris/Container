<?php
namespace Avris\Test;

use Avris\Container\ContainerBuilderExtension;
use Avris\Container\ContainerInterface;

class TestContainerBuilderExtension implements ContainerBuilderExtension
{
    public function extend(ContainerInterface $container)
    {
        $container->set('foo', 8);
    }
}
