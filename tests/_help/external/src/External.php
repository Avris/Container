<?php

namespace TestProjectExternal;

class External
{
    /** @var Dependency */
    private $dependency;

    public function __construct(Dependency $dependency)
    {
        $this->dependency = $dependency;
    }

    public function getValue()
    {
        return $this->dependency->getValue() * 10;
    }
}
