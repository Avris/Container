<?php

namespace Avris\Container;

use Avris\Container\Parameters\SimpleParameterProvider;
use Avris\Container\Service\ServiceDefinition;
use PHPUnit\Framework\TestCase;
use TestProjectCompiler\Bar;
use TestProjectCompiler\Excluded\AutowireDependency;
use TestProjectCompiler\Excluded\Config;
use TestProjectCompiler\Excluded\NotAutowireable;
use TestProjectCompiler\Excluded;
use TestProjectCompiler\Excluded\NotAutowireableClass;
use TestProjectCompiler\Foo;
use TestProjectCompiler\OsiemDependant;
use TestProjectCompiler\Thing\ThingManagerInterface;
use TestProjectExternal\Dependency;

/**
 * @covers \Avris\Container\ContainerCompiler
 */
class ContainerCompilerTest extends TestCase
{
    /** @var array */
    private $config;

    /** @var array */
    private $autoconfiguration;

    /** @var Container */
    private $container;

    /** @var ContainerCompiler */
    private $compiler;

    protected function setUp()
    {
        $this->config = require __DIR__ . '/_help/compiler/services.php';
        $this->autoconfiguration = require __DIR__ . '/_help/compiler/autoconfiguration.php';

        $this->container = new Container(new SimpleParameterProvider([
            'MODULE_DIR' => __DIR__ . '/_help/compiler/src',
            'VALUE' => 8,
        ]));

        $this->compiler = $this->buildCompiler();
    }

    private function buildCompiler()
    {
        return $this->compiler = new ContainerCompiler($this->container, $this->config, $this->autoconfiguration);
    }

    private function compile()
    {
        /** @var ServiceDefinition $definition */
        foreach ($this->compiler->compile() as $name => $definition) {
            if (!$this->container->has($name)) {
                $this->container->setDefinition($name, $definition);
            }
        }
    }

    public function testCompile()
    {
        $this->compile();

        $this->assertTrue($this->container->has(Foo::class));
        $this->assertTrue($this->container->has(Bar::class));

        $this->assertFalse($this->container->has(Excluded::class));

        $this->assertTrue($this->container->has(ThingManagerInterface::class));
        $this->assertCount(3, $this->container->getByTag('thing'));
    }

    /**
     * @expectedException \Avris\Container\Exception\AutowiringException
     * @expectedExceptionMessage Cannot autowire parameter $value of service TestProjectCompiler\Excluded\NotAutowireable
     */
    public function testCannotAutowire()
    {
        $this->config[NotAutowireable::class] = ['public' => true];
        $this->buildCompiler();
        $this->compile();
    }

    public function testCannotAutowireIgnored()
    {
        $this->config[NotAutowireable::class] = [];
        $this->buildCompiler();
        $this->compile();

        $this->assertFalse($this->container->has(NotAutowireable::class));
    }

    /**
     * @expectedException \Avris\Container\Exception\AutowiringException
     * @expectedExceptionMessage Cannot autowire parameter $thing of service TestProjectCompiler\Excluded\NotAutowireableClass. Either wire the value manually or create an alias (candidates:
     */
    public function testCannotAutowireClass()
    {
        $this->config[NotAutowireableClass::class] = ['public' => true];
        $this->buildCompiler();
        $this->compile();
    }

    public function testAutowireDependency()
    {
        $this->config[AutowireDependency::class] = ['public' => true];
        $this->buildCompiler();
        $this->compile();

        $service = $this->container->get(AutowireDependency::class);
        $this->assertEquals(880, $service->getValue());
    }

    public function testComplexUsage()
    {
        $this->config[OsiemDependant::class] = [
            'public' => true,
            'arguments' => [
                '$value' => 'OŚM-{@TestProjectCompiler\Osiem.value}',
            ],
        ];
        $this->buildCompiler();
        $this->compile();

        $service = $this->container->get(OsiemDependant::class);
        $this->assertEquals('OŚM-8', $service->getValue());
    }

    public function testConfig()
    {
        $this->container->set(Dependency::class, new Dependency());
        $this->container->set('config', [
            'foo' => [
                'bar' => [
                    'baz' => 'OK',
                ],
            ],
        ]);

        $this->config[Config::class] = [ 'public' => true ];
        $this->buildCompiler();
        $this->compile();

        $this->assertEquals(
            '8-OK',
            $this->container->get(Config::class)->getValue()
        );
    }

    public function testAliases()
    {
        $this->config['a'] = 'b';
        $this->config['b'] = 'c';
        $this->config['c'] = [
            'class' => Dependency::class,
            'public' => true,
        ];
        $this->buildCompiler();
        $this->compile();

        $this->assertEquals(
            8,
            $this->container->get('a')->getValue()
        );
    }

    public function testAliasesCleanup()
    {
        $this->config['a'] = 'b';
        $this->config['b'] = 'c';
        $this->config['c'] = [ 'class' => Dependency::class ];
        $this->buildCompiler();
        $this->compile();

        $this->assertFalse($this->container->has('a'));
    }
}
