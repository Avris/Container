<?php

namespace Avris\Container;

use Avris\Test\TestContainerBuilderExtension;
use PHPUnit\Framework\TestCase;

/**
 * @covers \Avris\Container\ContainerAssistedBuilder
 */
final class ContainerAssistedBuilderTest extends TestCase
{
    public function testBuild()
    {
        $value = (new ContainerAssistedBuilder())
            ->registerExtension(new TestContainerBuilderExtension())
            ->build('foo');

        $this->assertSame(8, $value);
    }
}
