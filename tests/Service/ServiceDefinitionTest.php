<?php

namespace Avris\Container\Service;

use App\Entity\Dependency;
use PHPUnit\Framework\TestCase;
use TestProjectExternal\External;

/**
 * @covers \Avris\Container\Service\ServiceDefinition
 */
class ServiceDefinitionTest extends TestCase
{
    public function testEmpty()
    {
        $definition = ServiceDefinition::fromArray(['unexpected' => 'is ignored']);

        $this->assertNull($definition->getName());
        $this->assertNull($definition->getClass());
        $this->assertSame([], $definition->getArguments());
        $this->assertSame(false, $definition->hasArgument('foo'));
        $this->assertNull($definition->getArgument('foo'));
        $this->assertSame([], $definition->getCalls());
        $this->assertSame([], $definition->getTags());
        $this->assertNull($definition->getDir());
        $this->assertSame([], $definition->getExclude());
        $this->assertSame(false, $definition->isFactory());
        $this->assertNull($definition->getResolve());
        $this->assertSame(false, $definition->isPublic());

        $this->assertSame(false, $definition->shouldStay());
        $this->assertEquals([], $definition->getUsages());

        $this->assertSame([], $definition->toArray());
    }

    public function testFromArray()
    {
        $array = [
            'class' => External::class,
            'arguments' => [
                '$foo' => '@bar',
            ],
            'calls' => [
                'setOsiem' => ['trzynaść'],
            ],
            'tags' => ['thing'],
            'factory' => false,
            'public' => true,
        ];

        $definition = ServiceDefinition::fromArray($array, 'srvs');

        $this->assertEquals('srvs', $definition->getName());
        $this->assertEquals(External::class, $definition->getClass());
        $this->assertEquals(['$foo' => '@bar'], $definition->getArguments());
        $this->assertSame(true, $definition->hasArgument('foo'));
        $this->assertEquals('@bar', $definition->getArgument('foo'));
        $this->assertEquals(['setOsiem' => ['trzynaść']], $definition->getCalls());
        $this->assertEquals(['thing'], $definition->getTags());
        $this->assertNull($definition->getDir());
        $this->assertSame([], $definition->getExclude());
        $this->assertSame(false, $definition->isFactory());
        $this->assertNull($definition->getResolve());
        $this->assertSame(true, $definition->isPublic());

        $this->assertSame(true, $definition->shouldStay());

        $this->assertEquals($array, $definition->toArray());
    }

    public function testDir()
    {
        $array = [
            'dir' => __DIR__,
            'exclude' => ['#^foo#'],
        ];

        $definition = ServiceDefinition::fromArray($array, 'dr');

        $this->assertNull($definition->getName());
        $this->assertNull($definition->getClass());
        $this->assertSame([], $definition->getArguments());
        $this->assertNull($definition->getArgument('foo'));
        $this->assertSame([], $definition->getCalls());
        $this->assertSame([], $definition->getTags());
        $this->assertEquals(__DIR__, $definition->getDir());
        $this->assertEquals(['#^foo#'], $definition->getExclude());
        $this->assertSame(false, $definition->isFactory());
        $this->assertNull($definition->getResolve());
        $this->assertSame(false, $definition->isPublic());

        $this->assertSame(false, $definition->shouldStay());

        $this->assertEquals($array, $definition->toArray());
    }

    public function testResolve()
    {
        $array = [
            'resolve' => '%VALUE%',
        ];

        $definition = ServiceDefinition::fromArray($array);

        $this->assertNull($definition->getClass());
        $this->assertSame([], $definition->getArguments());
        $this->assertNull($definition->getArgument('foo'));
        $this->assertSame([], $definition->getCalls());
        $this->assertSame([], $definition->getTags());
        $this->assertNull($definition->getDir());
        $this->assertSame([], $definition->getExclude());
        $this->assertSame(false, $definition->isFactory());
        $this->assertEquals('%VALUE%', $definition->getResolve());
        $this->assertSame(false, $definition->isPublic());

        $this->assertSame(false, $definition->shouldStay());

        $this->assertEquals($array, $definition->toArray());
    }

    public function testMerge()
    {
        $definition = ServiceDefinition::fromArray([
            'class' => External::class,
            'arguments' => [
                '$foo' => '@bar',
            ],
            'calls' => [
                'setOsiem' => ['trzynaść'],
            ],
            'tags' => ['thing'],
            'factory' => false,
            'public' => true,
        ]);

        $definition->merge([
            'class' => Dependency::class,
            'arguments' => [
                '$bar' => [8],
            ],
            'calls' => [
                'setDziew' => 'ięć',
            ],
            'tags' => ['other'],
            'factory' => true,
            'public' => false,
        ], 'newName');

        $this->assertEquals([
            'class' => Dependency::class,
            'arguments' => [
                '$foo' => '@bar',
                '$bar' => [8],
            ],
            'calls' => [
                'setOsiem' => ['trzynaść'],
                'setDziew' => 'ięć',
            ],
            'tags' => ['thing', 'other'],
            'factory' => true,
            'public' => false,
        ], $definition->toArray());

        $this->assertEquals('newName', $definition->getName());
    }

    public function testMergeEmpty()
    {
        $array = [
            'class' => External::class,
            'arguments' => [
                '$foo' => '@bar',
            ],
            'calls' => [
                'setOsiem' => ['trzynaść'],
            ],
            'tags' => ['thing'],
            'factory' => false,
            'public' => true,
        ];

        $definition = ServiceDefinition::fromArray($array);

        $definition->merge([]);

        $this->assertEquals($array, $definition->toArray());
    }

    public function testMergeDir()
    {
        $array = [
            'dir' => __DIR__,
            'exclude' => [
                '#^foo#',
            ],
        ];

        $definition = ServiceDefinition::fromArray($array);

        $definition->merge([
            'dir' => __DIR__ . '/src',
            'exclude' => [
                '#^bar#',
            ],
        ]);

        $this->assertEquals([
            'dir' => __DIR__ . '/src',
            'exclude' => [
                '#^foo#',
                '#^bar#',
            ],
        ], $definition->toArray());
    }

    public function testMergeEmptyWithResolve()
    {
        $definition = ServiceDefinition::fromArray([]);

        $definition->merge([
            'resolve' => 'foo',
        ]);

        $this->assertEquals([
            'resolve' => 'foo',
        ], $definition->toArray());
    }

    public function testCreateFromDir()
    {
        $array = [
            'dir' => __DIR__,
            'exclude' => [
                '#^foo#',
            ],
        ];

        $definition = ServiceDefinition::fromArray($array);

        $newDefinition = $definition->createFromDir(External::class);

        $this->assertNotSame($definition, $newDefinition);

        $this->assertEquals([
            'class' => External::class,
        ], $newDefinition->toArray());
    }

    public function testSetClassNotGiven()
    {
        $definition = ServiceDefinition::fromArray([]);

        $definition->setClassIfNotGiven(External::class);

        $this->assertEquals([
            'class' => External::class,
        ], $definition->toArray());
    }

    public function testSetClassGiven()
    {
        $definition = ServiceDefinition::fromArray(['class' => External::class]);

        $definition->setClassIfNotGiven('foo');

        $this->assertEquals([
            'class' => External::class,
        ], $definition->toArray());
    }

    public function testSetClassResolve()
    {
        $definition = ServiceDefinition::fromArray(['resolve' => 'foo']);

        $definition->setClassIfNotGiven(External::class);

        $this->assertEquals([
            'resolve' => 'foo',
        ], $definition->toArray());
    }

    public function testSetArgument()
    {
        $definition = ServiceDefinition::fromArray([]);

        $this->assertSame(false, $definition->hasArgument('foo'));
        $this->assertNull($definition->getArgument('foo'));
        $this->assertSame([], $definition->toArray());

        $definition->setArgument('foo', 'bar');

        $this->assertSame(true, $definition->hasArgument('foo'));
        $this->assertEquals('bar', $definition->getArgument('foo'));
        $this->assertEquals(['arguments' => [
            '$foo' => 'bar',
        ]], $definition->toArray());
    }

    public function testShouldStayNotUsed()
    {
        $definition = ServiceDefinition::fromArray([]);

        $this->assertFalse($definition->shouldStay());
    }

    public function testShouldStayPublic()
    {
        $definition = ServiceDefinition::fromArray(['public' => true]);

        $this->assertTrue($definition->shouldStay());
    }

    public function testShouldStayUsed()
    {
        $definition = ServiceDefinition::fromArray([]);
        $usage = ServiceDefinition::fromArray(['public' => true], 'name');

        $definition->used($usage);

        $this->assertTrue($definition->shouldStay());
        $this->assertSame(['name' => $usage], $definition->getUsages());
    }

    public function testShouldStayTagged()
    {
        $definition = ServiceDefinition::fromArray(['tags' => ['thing']]);

        $this->assertTrue($definition->shouldStay());
    }
}
