<?php
namespace Avris\Container;

use Avris\Bag\Bag;
use Avris\Container\Parameters\SimpleParameterProvider;
use Avris\Container\Service\ServiceDefinition;
use PHPUnit\Framework\TestCase;
use TestProjectContainer\Bar;
use TestProjectContainer\BrokenCall;
use TestProjectContainer\BrokenCall2;
use TestProjectContainer\BrokenDependency;
use TestProjectContainer\BrokenParam;
use TestProjectContainer\CircularA;
use TestProjectContainer\Config;
use TestProjectContainer\Database;
use TestProjectContainer\Factory;
use TestProjectContainer\Foo;
use TestProjectContainer\Resolver;
use TestProjectContainer\Thing\ThingManager;
use TestProjectContainer\Thing\ThingManagerInterface;
use TestProjectContainer\Xyz;

/**
 * @covers \Avris\Container\Container
 * @covers \Avris\Container\Parameters\SimpleParameterProvider
 */
final class ContainerTest extends TestCase
{
    private const CONFIG = [
        'foo' => 'FOO',
        'bar' => 'BAR',
    ];

    /** @var Container */
    private $container;

    protected function setUp()
    {
        $config = require __DIR__ . '/_help/container/services.php';
        $autoconfiguration = require __DIR__ . '/_help/container/autoconfiguration.php';

        $this->container = new Container(new SimpleParameterProvider([
            'MODULE_DIR' => __DIR__ . '/_help/container/src',
            'VALUE' => 8,
        ]));

        $this->container->set('config', self::CONFIG);

        $compiler = new ContainerCompiler($this->container, $config, $autoconfiguration);

        /** @var ServiceDefinition $definition */
        foreach ($compiler->compile() as $name => $definition) {
            if (!$this->container->has($name)) {
                $this->container->setDefinition($name, $definition);
            }
        }
    }

    public function testHas()
    {
        $this->assertTrue($this->container->has(''));
        $this->assertTrue($this->container->has('container'));
        $this->assertTrue($this->container->has(ContainerInterface::class));
        $this->assertTrue($this->container->has(Container::class));

        $this->assertTrue($this->container->has(Foo::class));
        $this->assertTrue($this->container->has(Bar::class));

        $this->assertFalse($this->container->has('nonexistent'));
        $this->assertFalse($this->container->has(Config::class));
    }

    /**
     * @expectedException \Avris\Container\Exception\NotFoundException
     * @expectedExceptionMessage Cannot directly access a private service "TestProjectContainer\Foo"
     */
    public function testGetPrivate()
    {
        $this->container->get(Foo::class);
    }

    public function testGetPublic()
    {
        $bar = $this->container->get(Bar::class);

        $this->assertEquals(23, $bar->getValue());
    }

    public function testGetResolve()
    {
        $value = $this->container->get('barValue');

        $this->assertEquals(23, $value);
    }

    public function testGetByTag()
    {
        $things = $this->container->getByTag('thing');

        $this->assertCount(3, $things);
    }

    public function testGetByTagEmpty()
    {
        $services = $this->container->getByTag('nonexistent');

        $this->assertSame([], $services);
    }

    public function testGetByInterfaceAlias()
    {
        $manager = $this->container->get(ThingManagerInterface::class);

        $this->assertEquals(3 * 8 + 13 * 13 + 7, $manager->getValue());
        $this->assertNull($manager->getOptional());
    }

    public function testGetConfig()
    {
        /** @var Config $config */
        $config = $this->container->get('ConfigClass');

        $this->assertInstanceOf(Bag::class, $config->getConfig());
        $this->assertEquals(self::CONFIG, $config->getConfig()->all());
        $this->assertEquals('FOO', $config->getRequired());
        $this->assertEquals('BAR', $config->getOptionalExistent());
        $this->assertNull($config->getOptionalNonexistent());
        $this->assertEquals('OŚM-FOO-8', $config->getAdditional());
    }

    /**
     * @expectedException \Avris\Container\Exception\NotFoundException
     * @expectedExceptionMessage Service "Nonexistent" not found (required by TestProjectContainer\BrokenDependency)
     */
    public function testGetNotFound()
    {
        $this->container->get(BrokenDependency::class);
    }

    /**
     * @expectedException \Avris\Container\Exception\ContainerException
     * @expectedExceptionMessage Call nonexistent does not exist for service TestProjectContainer\BrokenCall
     */
    public function testBrokenCall()
    {
        $this->container->get(BrokenCall::class);
    }

    /**
     * @expectedException \Avris\Container\Exception\ContainerException
     * @expectedExceptionMessage A call definition should be an array [method, [parameters, ...]] (service TestProjectContainer\BrokenCall2)
     */
    public function testBrokenCall2()
    {
        $this->container->get(BrokenCall2::class);
    }

    /**
     * @expectedException \Avris\Container\Exception\ContainerException
     * @expectedExceptionMessage Parameter $nonexistent is required for service TestProjectContainer\BrokenParam and cannot be autowired
     */
    public function testBrokenParam()
    {
        $this->container->setDefinition(BrokenParam::class, ServiceDefinition::fromArray([
            'class' => BrokenParam::class,
            'public' => true,
        ]));

        $this->container->get(BrokenParam::class);
    }

    public function getContainer()
    {
        $container1 = $this->container->get('');
        $container2 = $this->container->get('container');
        $container3 = $this->container->get(Container::class);
        $container4 = $this->container->get(ContainerInterface::class);

        $this->assertSame($this->container, $container1);
        $this->assertSame($this->container, $container2);
        $this->assertSame($this->container, $container3);
        $this->assertSame($this->container, $container4);
    }

    /**
     * @expectedException \Avris\Container\Exception\ContainerException
     * @expectedExceptionMessage Cannot overwrite container service
     */
    public function testCannotOverWriteContainer()
    {
        $this->container->set(ContainerInterface::class, 'foo');
    }

    /**
     * @expectedException \Avris\Container\Exception\ContainerException
     * @expectedExceptionMessage Cannot overwrite container service
     */
    public function testCannotOverWriteContainerDefinition()
    {
        $this->container->setDefinition(ContainerInterface::class, 'foo');
    }

    /**
     * @expectedException \Avris\Container\Exception\ContainerException
     * @expectedExceptionMessage Circular dependency detected between services: TestProjectContainer\CircularA, TestProjectContainer\CircularB
     */
    public function testCircular()
    {
        $this->container->get(CircularA::class);
    }

    public function testResolver()
    {
        $value = $this->container->get(Resolver::class);

        $this->assertEquals('OK-8', $value);
    }

    public function testSetArray()
    {
        $this->container->setDefinition(Xyz::class, [
            'arguments' => [
                '$value' => 15,
            ],
            'public' => true,
        ]);

        $service = $this->container->get(Xyz::class);
        $this->assertInstanceOf(Xyz::class, $service);
        $this->assertSame(15, $service->getValue());
    }

    public function testSetWithAlias()
    {
        $service = new Database();

        $this->container->setWithAlias($service, 'db');

        $this->assertSame($service, $this->container->get(Database::class));
        $this->assertSame($service, $this->container->get('db'));
    }

    public function testGetZero()
    {
        $this->assertSame(0, $this->container->get('zero'));
        $this->assertSame(0, $this->container->get('zero')); // Repetition on purpose: only second call causes edgecase
    }

    public function testRestart()
    {
        $this->container->setDefinition('x', ServiceDefinition::fromArray(['resolve' => 'foo', 'public' => true]));
        $this->assertEquals('foo', $this->container->get('x'));

        $this->container->set('x', 'bar');
        $this->assertEquals('bar', $this->container->get('x'));

        $this->container->restart('x');
        $this->assertEquals('foo', $this->container->get('x'));
    }

    public function testDelete()
    {
        $this->container->set('x', 1, ['tmp']);
        $this->container->set('y', 2, ['tmp']);

        $this->assertTrue($this->container->has('x'));
        $this->assertTrue($this->container->has('y'));
        $this->assertEquals(
            1,
            $this->container->get('x')
        );
        $this->assertEquals(
            ['x' => 1, 'y' => 2],
            $this->container->getByTag('tmp')
        );

        $this->container->set('x', null);
        $this->container->setDefinition('y', null);

        $this->assertFalse($this->container->has('x'));
        $this->assertFalse($this->container->has('y'));
        $this->assertEquals(
            [],
            $this->container->getByTag('tmp')
        );
    }

    public function testClear()
    {
        $this->container->clear();

        $this->assertFalse($this->container->has(Foo::class));
        $this->assertFalse($this->container->has(Bar::class));
        $this->assertFalse($this->container->has(CircularA::class));
        $this->assertFalse($this->container->has(ThingManager::class));
        $this->assertSame([], $this->container->getByTag('thing'));
    }

    public function testGetParameter()
    {
        $this->assertEquals(
            8,
            $this->container->getParameter('VALUE')
        );
    }

    /**
     * @expectedException \Avris\Container\Exception\NotFoundException
     * @expectedExceptionMessage Undefined parameter "NONEXISTENT"
     */
    public function testGetParameterNonexistent()
    {
        $this->container->getParameter('NONEXISTENT');
    }

    public function testFactory()
    {
        $this->assertNotSame(
            $this->container->get(Factory::class)->getValue(),
            $this->container->get(Factory::class)->getValue()
        );
    }
}
